<?php
namespace Nora\Kvs;

use Nora\DataStructure\StructedObject;
use Nora\DataStructure\Schema;

class StorageContext extends StructedObject
{
    public static function schema(Schema $params)
    {
        $params->type = Schema::string([
            'required' => true
        ]);
        $params->name = Schema::string([
            'required' => true
        ]);
    }
}
