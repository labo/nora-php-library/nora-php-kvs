<?php
namespace Nora\Kvs;

class StorageFactory
{
    public static function create(StorageContext $context)
    {
        $type = sprintf(
            __NAMESPACE__."\\Storage\\Storage%s",
            ucfirst($context->type)
        );

        $storage = new $type($context);
        return $storage;
    }
}
