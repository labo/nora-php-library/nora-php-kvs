<?php
namespace Nora\Kvs\Storage;

use Nora\Kvs\StorageContext;

abstract class AbstractStorage
{
    public function __construct(StorageContext $context)
    {
        $this->initialize($context);
    }

    abstract public function saveItem($name, $item);
    abstract public function getItem($name);
    abstract public function delItem($name);
    abstract public function hasItem($name) : bool;
    abstract protected function initialize(StorageContext $context);

    protected function serialize($data)
    {
        return serialize($data);
    }

    protected function unserialize($data)
    {
        return unserialize($data);
    }
}
