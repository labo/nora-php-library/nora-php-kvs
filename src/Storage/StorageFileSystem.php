<?php
namespace Nora\Kvs\Storage;

use Nora\Kvs\StorageContext;

class StorageFileSystem extends AbstractStorage
{
    private $path;

    protected function initialize(StorageContext $context)
    {
        // Name as path
        $this->path = $context->name;
    }

    public function saveItem($name, $item)
    {
        $key = $this->nameToPath($name);
        file_put_contents(
            $this->nameToPath($name),
            $this->serialize($item)
        );

        return $this->nameToPath($name);
    }

    public function getItem($name)
    {
        if ($this->hasItem($name)) {
            return $this->unserialize(file_get_contents($this->nameToPath($name)));
        }
        return null;
    }

    public function delItem($name)
    {
        if ($this->hasItem($name)) {
            unlink($this->nameToPath($name));
        }
    }

    public function hasItem($name) : bool
    {
        $key = $this->nameToPath($name);
        return file_exists($key);
    }

    private function nameToPath(string $name): string
    {
        return $this->path .'/'. $name;
    }

}
