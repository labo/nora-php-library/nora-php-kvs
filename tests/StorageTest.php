<?php
declare(strict_types=1);

namespace Nora\Kvs;

use PHPUnit\Framework\TestCase;

class KvsTest extends TestCase
{
    /**
     * @test
     */
    public function KvsTest()
    {
        $context = new StorageContext([
            'type' => 'FileSystem',
            'name' => '/tmp'
        ]);

        $storage = StorageFactory::create($context);

        $this->assertFalse($storage->hasItem('hoge'));

        $this->assertEquals('/tmp/hoge', $storage->saveItem('hoge', (object) [
            'a' => 'b',
            'c' => 'd'
        ]));

        $this->assertTrue($storage->hasItem('hoge'));

        $this->assertEquals('b', $storage->getItem('hoge')->a);

        $storage->delItem('hoge');

        $this->assertFalse($storage->hasItem('hoge'));
    }
}
