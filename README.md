# Key Value Store

install
-------------------------------

```
composer install avap/nora-php-kvs
````


Usage
---------------------------------

### FileSystem

```php
Nora\Kvs\StorageFactory::create(
  new Nora\Kvs\StorageContext([
    'type' => 'filesystem',
    'name' => '/tmp/storage'
  ])
)
```

### S3

```php
Nora\Kvs\StorageFactory::create(
  new Nora\Kvs\StorageContext([
    'type' => 's3',
    'name' => 'testbucket/tests'
  ])
)
```

### Dynamo

```php
Nora\Kvs\StorageFactory::create(
  new Nora\Kvs\StorageContext([
    'type' => 'dynamo',
  ])
)
```
